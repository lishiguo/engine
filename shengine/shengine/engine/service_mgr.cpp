#include "service_mgr.h"
#include "script_engine.h"
#include "lua_service.h"
#include "time_mgr.h"
#include "c_msg.h"
#include "script_msg.h"

ServiceMgr gServiceMgr;

ServiceMgr::ServiceMgr() : threadNum_(1), threadCursor_(0), exitTime_(0){
	setEntryFunc("entry__");
	setLuaClass("service_mgr");
	setName("service_mgr");
	registerMsgHandle(reqNewLuaService);
	registerMsgHandle(reqRemoveService);
	registerMsgHandle(reqExitProcess);
}

ServiceMgr::~ServiceMgr() {
	for (int32 i = 0; i < (int32)threadList_.size(); i++) {
		if (threadList_[i]) {
			delete threadList_[i];
		}
	}
	threadList_.clear();
}

void ServiceMgr::init(int32 threadNum) {
	shengine_assert(threadNum > 0);
	threadNum_ = threadNum;
	setHandle(Guid64(APPID, SERVICE_MRR, 0));
}

 void ServiceMgr::run() {
	addBaseService(this);
	baseThread_.Thread::start();

	for (int32 i = 0; i < threadNum_; i++) {
		GameThread* thread = new GameThread;
		thread->Thread::start();
		threadList_.push_back(thread);
	}

	while (!checkThreadsState(THREAD_STATE_READIED)) {
		Sleep(4);
	}

	SetThreadsState(THREAD_STATE_RUNNING);

	//todo to load game create script !
	//create script service
}

 void ServiceMgr::processBase() {
	 gMessageMgr.process();
	 gTimeMgr.process();
	 checkExit();
 };

 void ServiceMgr::processFrame() {
	 scriptCall("process");
 }

 void ServiceMgr::checkExit() {
	 if (exitTime_ > 0) {
		 if (serviceMap_.size() == 1) {
			 shutdown();
		 }
	 }
 }

void ServiceMgr::shutdown() {
	SetThreadsState(THREAD_STATE_EXIT);
	exitTime_ = gTimeMgr.getTime() + MAINTHREAD_EXIT_WAIT_TIME;
}

bool ServiceMgr::isRun() {
	return exitTime_ == 0 || gTimeMgr.getTime() >= exitTime_;
}

bool ServiceMgr::checkThreadsState(int32 st) {
	if (baseThread_.state() == st) {
		for (int32 i = 0; i < (int32)threadList_.size(); i++) {
			if (threadList_[i]->state() != st) {
				return false;
			}
		}
		return true;
	}
	return false;
}

bool ServiceMgr::SetThreadsState(int32 st) {
	baseThread_.setState(st);
	for (int32 i = 0; i < (int32)threadList_.size(); i++) {
		threadList_[i]->setState(st);
	}
	return true;
}

bool ServiceMgr::kick(const Guid64& handle) {
	lock_.lock();
	ServiceMap::iterator itr = serviceMap_.find(handle);
	if (itr != serviceMap_.end()) {
		serviceMap_.erase(itr);
		lock_.unlock();
		return true;
	}
	return false;
	lock_.unlock();
}

bool ServiceMgr::addService(Service* s) {
	shengine_assert(s);
	lock_.lock();
	ServiceMap::iterator itr = serviceMap_.find(s->getHandle());
	if (itr == serviceMap_.end()) {
		serviceMap_.insert(std::make_pair(s->getHandle(), s));
		lock_.unlock();
		gMessageMgr.addMsgQueue(s->getHandle(), s->getMsgQueue());
		threadList_[threadCursor_++ % threadList_.size()]->addService(s); //todo add from msg
		return true;
	}
	lock_.unlock();
	return false;
}

bool ServiceMgr::addBaseService(Service* s) {
	shengine_assert(s);
	ServiceMap::iterator itr = serviceMap_.find(s->getHandle());
	if (itr == serviceMap_.end()) {
		serviceMap_.insert(std::make_pair(s->getHandle(), s));
		gMessageMgr.addMsgQueue(s->getHandle(), s->getMsgQueue());
		baseThread_.addService(s); //todo add from msg
		return true;
	}
	shengine_assert(0);
	return false;
}

void ServiceMgr::reqNewLuaService(ServiceMgr& self, StreamReader& params) {
	uint64 retHandle;
	uint64 coid;
	uint64 newHandle;
	std::string clsName;
	uint64 processInterval;
	params >> retHandle >> coid >> newHandle >> clsName >> processInterval;

	LuaService* s = new LuaService;
	s->setEntryFunc(self.getEntryFunc());
	s->setHandle(newHandle);
	s->setLuaClass(clsName);
	s->setName(clsName);
	s->SetProcessInterval(processInterval);
	self.addService(s);

	CMessage msg("reqRetCoroutine");
	msg << (uint8)2;
	msg << (uint8)MSGPARAM_TINTEGER;
	msg << coid;
	msg << (uint8)MSGPARAM_TINTEGER;
	msg << newHandle;
	gMessageMgr.sendMessage(retHandle, msg.msg());
}

void ServiceMgr::reqRemoveService(ServiceMgr& self, StreamReader& params) {
	uint64 removeHandle;
	params >> removeHandle;
	ServiceMap::iterator itr = self.serviceMap_.find(removeHandle);
	if (itr != self.serviceMap_.end()) {
		gMessageMgr.removeMsgQueue(removeHandle);
		delete itr->second;
		self.serviceMap_.erase(itr);
	}
	else {
		shengine_assert(0);
	}
}

void ServiceMgr::reqExitProcess(ServiceMgr& self, StreamReader& params) {
	if (self.exitTime_ == 0) {
		//能到这里说明服务器正常运行，不可能处于暂停状态
		ServiceMap::iterator itr = self.serviceMap_.begin();
		CMessage msg("reqKickService");
		for (; itr != self.serviceMap_.end(); ++itr) {
			if (itr->first != self.getHandle().getValue()) {
				CMessage msg("reqKickService");
				gMessageMgr.sendMessage(itr->first, msg.msg());
			}
		}
		self.exitTime_ = gTimeMgr.getTime();
	}
}

