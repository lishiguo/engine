#ifndef __CHANNEL__
#define __CHANNEL__

#include "base.h"
#include "message.h"
#include "guid.h"

class MessageMgr;

class Channel
{
public:
	Channel() {}

	~Channel() {}

	void sendMessage(const Guid64& handle, Message* msg);

private:
	CacheMsgList cacheMsgList_;
};

#endif
