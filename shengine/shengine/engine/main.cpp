// shengine.cpp : 定义控制台应用程序的入口点。
//

#include <string>
#include "guid.h"
#include "service_mgr.h"

void startEngine()
{
	gServiceMgr.init(6);
	gServiceMgr.run();

	while (gServiceMgr.isRun())
	{
		Sleep(1000);
	}
}

int main()
{
	startEngine();

	return 0;
}

//void testSendCMsg(MessageService* ms) {
//	uint8 ftype = MSG_TC;
//	std::string fname = "reqFunc~";
//	int32 p1 = rand() % 1000000;
//	bool p2 = rand() % 2 == 1;
//	std::string p3 = "333p3";
//	float32 p4 = 33.66f;
//	uint64 now = TimeMgr().getTimeMs();
//	DataStream* stream = new DataStream;
//	(*stream) << ftype << fname << p1 << p2 << p3 << p4 << now;
//	ms->sendMessage(1001, stream);
//}
//
//void testSendLuaMsg(MessageService* ms, const Guid64& handle) {
//	uint8 ftype = MSG_TLUA;
//	std::string fname = "luaf";
//	uint64 p1 = rand() % 1000000;
//	bool p2 = rand() % 2 == 1;
//	std::string p3 = "333p3";
//	float64 p4 = 33.66f;
//	uint64 now = TimeMgr().getTimeMs();
//	const char* cosnt_str = "const_str";
//	DataStream* stream = new DataStream;
//	(*stream) << ftype << fname 
//	<< (uint8)8
//	<< (uint8)MSGPARAM_TINTEGER << p1
//	<< (uint8)MSGPARAM_TBOOL << p2
//	<< (uint8)MSGPARAM_TSTRING << p3
//	<< (uint8)MSGPARAM_TNUMBER << p4
//	<< (uint8)MSGPARAM_TINTEGER << now
//	<< (uint8)MSGPARAM_TSTRING << cosnt_str
//	<< (uint8)MSGPARAM_TLIGHTUSERDATA << stream
//	<< (uint8)MSGPARAM_TTABLE << 2 
//		<< (uint8)MSGPARAM_TSTRING  << "name" << (uint8)MSGPARAM_TSTRING <<"Jack" 
//		<< (uint8)MSGPARAM_TSTRING << "age" << (uint8)MSGPARAM_TINTEGER << (uint64)18;
//	ms->sendMessage(handle, stream);
//}