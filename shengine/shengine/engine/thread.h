#ifndef __THREAD__
#define __THREAD__

#include "base.h"
#include <windows.h>
#include "guid.h"
#include "lua_vm.h"
#include "service.h"

class Thread {
	typedef std::unordered_map<uint64, Service*> ServiceMap;
public:
	Thread() : handle_(0), threadId_(0) , state_(THREAD_STATE_INIT){
	}

	virtual ~Thread();

	void start();

	bool addService(Service* s);

	inline int32 state() {
		return state_;
	}

	inline void setState(int32 st) {
		state_ = st;
	}

	bool join();

	virtual bool runFrame();

protected:
	virtual void prepare() {};

	void checkNewService();

	void checkThreadState();
	
	bool removeService(const Guid64& handle);

protected:
	HANDLE handle_;
	DWORD threadId_;
	ServiceMap serviceMap_;
	ServiceMap serviceCacheMap_;
	int32 state_;
	std::mutex lock_;
};

#endif
