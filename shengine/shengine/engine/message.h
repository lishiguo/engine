#ifndef __MESSAGE__
#define __MESSAGE__

#include "base.h"
#include <mutex>
#include "guid.h"
#include "data_stream.h"

typedef DataStream Message;
typedef std::vector<Message*> MsgList;

struct CacheMessage {
	Guid64 handle_;
	Message* msg_;
};
typedef std::vector<CacheMessage> CacheMsgList;

class MessageQueue
{
public:
	MessageQueue() {}
	~MessageQueue() {}
	
	inline MsgList& getMsgList() {
		return msgList_;
	}
	
	inline void push(Message* msg) {
		lock_.lock();
		cacheMsgList_.push_back(msg);
		lock_.unlock();
	}

	inline void push(MsgList& msgList) {
		lock_.lock();
		msgList_.insert(msgList_.end(), msgList.begin(), msgList.end());
		lock_.unlock();
		msgList.clear();
	}
	
	void flush() {
		lock_.lock();
		msgList_.swap(cacheMsgList_);
		lock_.unlock();
	}
private:
	std::mutex lock_;
	MsgList msgList_;
	MsgList cacheMsgList_;
};

#endif
