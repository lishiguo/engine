#ifndef __SCRIPT_MSG__
#define __SCRIPT_MSG__

#include "message.h"
#include "script_stream_writer.h"

class ScriptMessage : public ScriptStreamWriter {
	typedef ScriptStreamWriter super;
	typedef super::super ssuper;
public:
	explicit ScriptMessage(const char* msgName) {
		shengine_assert(msgName);
		msgName = msgName ? msgName : "";
		ssuper::operator<< ((uint8)MSG_TLUA) << msgName;
	}

	inline Message* createMsg() {
		return createStreamReader().dataStream();
	}

	inline DataStream* dataStream() {
		return &dataStream_;
	}
};

#endif

