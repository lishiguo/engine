#include "channel.h"
#include "message_service.h"

void Channel::sendMessage(const Guid64& handle, Message* msg) {
	shengine_assert(msg);
	if (msg) {
		gMessageMgr.sendMessage(handle, msg);
	}
}
