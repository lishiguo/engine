#ifndef __ADDRESS__
#define __ADDRESS__

#include "base.h"

struct Address
{
	std::string ip_;
	uint16 port_;
};

#endif
