#ifndef __GUID__
#define __GUID__

#include "base.h"

class Guid64 {
public:
	Guid64() : value_(-1) {}

	Guid64(uint64 value) : value_(value) {}

	Guid64(uint16 appid, uint16 type, uint32 serial) {
		value_ = ((uint64)appid << 48) | ((uint64)type << 32) | ((uint64)serial);
	}

	inline uint16 getAppid() const {
		return (uint16)(value_ >> 48);
	}

	inline uint16 getType() const {
		return  (int32)(value_ >> 32) & 0xffff ;
	}

	inline uint32 getSerial() const {
		return (uint32)value_;
	}

	inline uint64 getValue() const {
		return value_;
	}

	inline operator int64() const {
		return (int64)value_;
	}

	inline operator uint64() const {
		return value_;
	}

private:
	uint64 value_;
};

#endif
