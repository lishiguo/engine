#ifndef __SCRIPT_ENGINE__
#define __SCRIPT_ENGINE__

#include "base.h"
#include "lua_vm.h"

class ScriptEngine : public LuaVm
{
public:
	ScriptEngine();
	virtual ~ScriptEngine();

	virtual void loadGameLibs();
};

#endif
