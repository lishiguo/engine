#ifndef __BASE_DEFINE__
#define __BASE_DEFINE__

#if defined(__WIN32__)
#include "windows.h"
#define __sync_lock_test_and_set(plock, v) InterlockedExchange(plock, v)
#define __sync_lock_release(plock) InterlockedExchange(plock, 0)
#define __sync_add_and_fetch(plock, v) InterlockedIncrement(plock)
#define __sync_sub_and_fetch(plock, v) InterlockedDecrement(plock)
typedef long AtomLock;
typedef long AtomRef;
#define THREAD_ID unsigned long
#else
typedef int32 AtomLock;
typedef int32 AtomRef;
#define THREAD_ID pthread_t
#endif

#define ATOM_LOCK(L) while(__sync_lock_test_and_set(L, 1));
#define ATOM_UNLOCK(L) __sync_lock_release(L);
#define ATOM_ADD(N, v) __sync_add_and_fetch(N, v);
#define ATOM_SUB(N, v) __sync_sub_and_fetch(N, v);
#define REF_RETAIN(R) __sync_add_and_fetch(R, 1);
#define REF_RELEASE(R) __sync_sub_and_fetch(R, 1);

#define MSG_TC 0
#define MSG_TLUA 1

#define MSGPARAM_TINVALID -1
#define MSGPARAM_TBOOL 0
#define MSGPARAM_TNUMBER 1
#define MSGPARAM_TINTEGER 2
#define MSGPARAM_TSTRING 3
#define MSGPARAM_TLIGHTUSERDATA 4
#define MSGPARAM_TTABLE 5

//singleton service
#define SERVICE_MRR 1
#define THREAD_SERVICE 2
#define TIME_SERVICE 3
#define MESSAGE_SERVICE 4
#define LOGIN_SERVICE 5
#define SCENE_MGR 6
#define GATE_SERVICE 7
#define CROSS_SERVICE 8
//multi service
#define DB_SERVICE 9
#define SCENE_CELL_SERVICE 10
#define ACTOR_SERVICE 11

#define APPID 103

#define SERVICE_STATE_INIT 0
#define SERVICE_STATE_START 1
#define SERVICE_STATE_RUNNING 2
#define SERVICE_STATE_TOEXIT 3
#define SERVICE_STATE_EXITING 4
#define SERVICE_STATE_EXIT 5

#define SERVICE_DEFAULT_INTERVAL 4
#define THREAD_INTERVAL 4

#define THREAD_STATE_INIT 0
#define THREAD_STATE_READIED 1
#define THREAD_STATE_RUNNING 2
#define THREAD_STATE_TOPAUSE 3
#define THREAD_STATE_PAUSE 4
#define THREAD_STATE_TOEXIT 5
#define THREAD_STATE_EXIT 6

#define MAX_EXIT_WAIT_TIME 60
#define MAINTHREAD_EXIT_WAIT_TIME 1

#define shengine_assert assert

#define check_str(f) assert(f);f = f ? f : "";

#endif
