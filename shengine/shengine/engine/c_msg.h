#ifndef __C_MSG__
#define __C_MSG__

#include "message.h"
#include "stream_writer.h"

class CMessage : public StreamWriter {
public:
	explicit CMessage(const char* msgName) {
		check_str(msgName);
		(*this) << (uint8)MSG_TC << msgName;
	}

	inline Message* msg() {
		return dataStream();
	}
};

#endif
