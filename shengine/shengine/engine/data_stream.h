#ifndef __DATA_STREAM__
#define __DATA_STREAM__

#include "base.h"

struct DataStream {
	DataStream() : ref_(0) { stream_.reserve(128); }
	DataStream(const DataStream&) = delete;
	void operator=(const DataStream&) = delete;

	inline void retain() {
		REF_RETAIN(&ref_);
	}

	inline void release() {
		shengine_assert(ref_ > 0);
		if (ref_ > 0) {
			REF_RELEASE(&ref_);
			if (ref_ == 0) {
				delete this;
			}
		}
	}

	std::vector<uint8> stream_;
	AtomRef ref_;
};

#endif
