#ifndef __TIMER_MGR__
#define __TIMER_MGR__

#include "base.h"

class TimeMgr {
	friend class ServiceMgr;
public:
	TimeMgr() {
		process();
	}

	inline uint64 getTime() {
		return timeS_;
	}

	inline uint64 getTimeMs() {
		return timeMs_;
	}

private:
	void process();

private:
	uint64 timeS_;
	uint64 timeMs_;
};

extern TimeMgr gTimeMgr;

#endif
