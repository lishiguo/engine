#ifndef __SERVICE_MGR__
#define __SERVICE_MGR__

#include "lua_service.h"
#include "game_thread.h"
#include "message_service.h"

class ServiceMgr : public LuaService {
	typedef std::unordered_map<uint64, Service*>  ServiceMap;
	typedef std::vector<Thread*> ThreadList;
public:
	ServiceMgr();

	~ServiceMgr();

	void init(int32 threadNum);

	void run();

	void checkExit();

	void shutdown();

	bool isRun();

	bool kick(const Guid64& handle);

	bool addBaseService(Service* s);

	bool addService(Service* s);

	virtual bool autoStart() { return true; }
private:
	virtual void processBase();

	virtual void processFrame();

	bool checkThreadsState(int32 st);

	bool SetThreadsState(int32 st);
private:
	static void reqNewLuaService(ServiceMgr& self, StreamReader& params);

	static void reqRemoveService(ServiceMgr& self, StreamReader& params);

	static void reqExitProcess(ServiceMgr& self, StreamReader& params);
private:
	std::mutex lock_;
	ServiceMap serviceMap_;
	int32 threadNum_;
	int32 threadCursor_;
	ThreadList threadList_;
	GameThread baseThread_;
	uint64 exitTime_;
};

extern ServiceMgr gServiceMgr;

#endif
