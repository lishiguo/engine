#ifndef __LUA_SERVICE__
#define __LUA_SERVICE__

#include "service.h"
#include "lua_vm.h"
#include "message.h"

class LuaService : public Service 
{
	typedef Service super;
public:
	LuaService();

	virtual ~LuaService(){}

public:
	virtual void onAllocThread(Thread* thread);

	virtual void onRemoveFromThread();

	void processFrame();

	bool handleMessage(uint8 funcType, const char* funcName, StreamReader& params);

protected:
	bool scriptCall(const char* f, StreamReader* params = NULL, StreamWriter* rets = NULL);
	bool scriptCallEx(const char* f, StreamReader* params = NULL, StreamWriter* rets = NULL);
	bool scriptCallRaw(const char* f, StreamReader* params = NULL, StreamWriter* rets = NULL);
	bool scriptCallRawEx(const char* f, StreamReader* params = NULL, StreamWriter* rets = NULL);
public:
	inline void setLuaVm(LuaVm* luaVm) {
		luaVm_ = luaVm;
	}

	inline LuaVm* scriptEngine() {
		return luaVm_;
	}

	inline void setEntryFunc(const std::string& f) {
		entryFunc_ = f;
	}

	inline const std::string& getEntryFunc() const {
		return entryFunc_;
	}

	inline void setLuaClass(const std::string& cls) {
		luaClass_ = cls;
	}

	inline const std::string& getLuaClass() const {
		return luaClass_;
	}
protected:
	virtual void start();

	virtual bool finalsave();

private:
	static void reqRemoteScriptCall(LuaService& self, StreamReader& params);
	static void reqRetCoroutine(LuaService& self, StreamReader& params);
	static void reqKickService(LuaService& self, StreamReader& params);
	static void retFinalSaveOk(LuaService& self, StreamReader& params);
private:
	LuaVm* luaVm_;
	std::string entryFunc_;
	std::string luaClass_;
};

#endif
