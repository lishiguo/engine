#ifndef __SCRIPT_STREAM_WRITER__
#define __SCRIPT_STREAM_WRITER__

#include "stream_writer.h"
#include "message.h"

class ScriptStreamWriter : protected StreamWriter {
protected:
	typedef StreamWriter super;
public:
	ScriptStreamWriter() : sizeOffset_(0), size_(0) {
		super::operator<< ((uint8)0);
		sizeOffset_ = length() - sizeof(uint8);
	}

	inline ScriptStreamWriter& operator << (bool v) {
		super::operator<<((uint8)MSGPARAM_TBOOL) << (v);
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (int64 v) {
		super::operator<<((uint8)MSGPARAM_TINTEGER) << v;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (uint64 v) {
		super::operator<<((uint8)MSGPARAM_TINTEGER) << v;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (const Guid64& v) {
		super::operator<<((uint8)MSGPARAM_TINTEGER) << v;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (float64 v) {
		super::operator<<((uint8)MSGPARAM_TNUMBER) << v;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (const std::string& str) {
		super::operator<<((uint8)MSGPARAM_TSTRING) << str;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (const char* str) {
		shengine_assert(str);
		str = str ? str : "";
		super::operator<<((uint8)MSGPARAM_TSTRING) << str;
		++size_;
		return *this;
	}

	inline ScriptStreamWriter& operator << (void* udata) {
		super::operator<<((uint8)MSGPARAM_TLIGHTUSERDATA) << udata;
		++size_;
		return *this;
	}

	inline StreamReader createStreamReader() {
		*(uint8*)(stream_.data() + sizeOffset_) = size_;
		return super::createStreamReader();
	}

private:
	int32 sizeOffset_;
	int32 size_;
};

typedef ScriptStreamWriter ScriptParams;
typedef StreamWriter ScriptRets;

#endif
