#ifndef __GAME_THREAD__
#define __GAME_THREAD__

#include "thread.h"
#include "script_engine.h"

class GameThread : public Thread {
public:
	GameThread() {}

	inline LuaVm* getLuaVm() {
		return &luaVm_;
	}

private:
	virtual void prepare() {
		luaVm_.setEntry("./script/main.lua");
		luaVm_.load();
	}

private:
	ScriptEngine luaVm_;
};

#endif
