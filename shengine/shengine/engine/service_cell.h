#ifndef __SERVICE_CELL__
#define __SERVICE_CELL__

#include "base.h"
#include "service.h"
#include "entity.h"

class ServiceCell : public Service {
	typedef std::vector<Entity*> EntityList;
public:
	ServiceCell() {}
	virtual ~ServiceCell() {}

	void shutdown() {
		
	}

protected:
	EntityList entityList_;
};

#endif
