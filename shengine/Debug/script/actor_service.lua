local super = require "service"
local db_service = require "db_service"
local shengine = require "shengine"
local actor_service = inherit(super, "actor_service")
local self = actor_service
local handle_mgr = require "handle_mgr"
local db_handle = handle_mgr.gen_dbhandle() --todo alloc db_handle from service_mgr 

function actor_service.start(handle)
	super.start(handle, self)
	shengine.fork(handle, function()
		local data = shengine.get_data(handle)
		print "actor_service start load data from db..."
		local dbdata = shengine.call(db_handle, "query", handle)	
		print "actor_service load data from db ok !"
		data._RUN = true
	end)
end

function actor_service.process(handle)
	local data = shengine.get_data(handle)
	if data and data._RUN then
		local a = 0
	end
end

function actor_service.req_querydata(handle, ms)
	--print("==>.cost " .. tostring(shengine.tick() - ms) .. " ms")
	if shengine.tick() - ms > 0 then
		--print("==>.cost " .. tostring(shengine.tick() - ms) .. " ms")
		--shengine.testlock(true)
	else
		--shengine.testlock(false)
	end
	--[[for i =1,200 do
		local a = 51212*1125.3 + 1212/121.2 + 1121*666
	end--]]
	return 12,333,"str..."
end

function actor_service.req_testmsg(handle, other_handle, ms)
	if shengine.tick() - ms > 0 then
		--print("==>===>.cost " .. tostring(shengine.tick() - ms) .. " ms")
	end
	shengine.fork(handle, function()
		local ret1,ret2,ret3 = shengine.call(other_handle, "req_querydata", shengine.tick())
		local aa = 0
	end)
end

shengine.register_class(actor_service)

return actor_service
