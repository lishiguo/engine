local handle_mgr = {}
local self = handle_mgr

local handle_type = {
--singleton service
	SERVICE_MRR = 1,
	THREAD_SERVICE = 2,
	TIME_SERVICE = 3,
	MESSAGE_SERVICE = 4,
	LOGIN_SERVICE = 5,
	SCENE_MGR = 6,
	GATE_SERVICE = 7,
	CROSS_SERVICE = 8,
--multi service
	DB_SERVICE = 9,
	SCENE_CELL_SERVICE = 10,
	ACTOR_SERVICE = 11,
}

handle_mgr.srvmgr_handle = handle_type.SERVICE_MRR
handle_mgr.time_handle = handle_type.TIME_SERVICE
handle_mgr.msg_handle = handle_type.MESSAGE_SERVICE
handle_mgr.login_handle = handle_type.LOGIN_SERVICE
handle_mgr.scenemgr_handle = handle_type.SCENE_MGR
handle_mgr.gate_handle = handle_type.GATE_SERVICE
handle_mgr.cross_handle = handle_type.CROSS_SERVICE

local acror_handle_serial_ = 1
function handle_mgr.gen_actorhandle()
	--todo call c function
	local shengine = require "shengine"
	local new_handle = shengine.create_guid(APPID, handle_type.ACTOR_SERVICE, acror_handle_serial_)
	acror_handle_serial_ = acror_handle_serial_ + 1
	return new_handle
end

function handle_mgr.gen_dbhandle()
	local shengine = require "shengine"
	return shengine.create_guid(APPID, handle_type.DB_SERVICE, 0)
end

function handle_mgr.gen_scenecellhandle()
	local shengine = require "shengine"
	return shengine.create_guid(APPID, handle_type.SCENE_CELL_SERVICE, 0)
end

return handle_mgr
