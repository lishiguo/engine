local super = require "service"
local shengine = require "shengine"
local db_service = inherit(super, "db_service")
local self = db_service

function db_service.start(handle)
	super.start(handle, self)
	print "function actor_service.start"
	return true,"ok"
end

function db_service.process(handle)
	--print "db_service.process"
end

function db_service.query(handle)
	print "db_service.query"
	return {name='Jack', age=18, score = 99.5}
end

function db_service.finalsave(handle)
	--shengine.exit(handle)
	return true
end

shengine.register_class(db_service)

return db_service

