local service = {}
local self = service
local shengine = require "shengine"

function service.start(handle, derived_cls)
	shengine.create_data(handle, derived_cls._NAME)
end

function service.process(handle)

end

function service.finalsave(handle)
	return true
end

function service.stop(handle)
	self.remove_data(handle)
end

function service.register_packet_handle(sysid, cmdid, f)
	if not self._PACKET_HANDLE[sysid] then
		self._PACKET_HANDLE[sysid] = {}
	end
	assert(self._PACKET_HANDLE[sysid][cmdid] == nil)
	self._PACKET_HANDLE[sysid][cmdid] = f
end

function service.handle_packet(handle, sysid, cmdid, ...)
	self._PACKET_HANDLE[sysid][cmdid](handle, ...)
end

function service.ret_coroutine(handle, coid, ...)
	local co_list = shengine._get_co_data(handle)
	assert(co_list[coid])
	assert(running_thread == 0)
	--assert(running_handle == 0)
	running_thread = coid
	running_handle = handle
	local ret,err = coroutine.resume(co_list[coid], ...)
	if not ret then
		co_list[coid] = nil
		print("Error: service.ret_coroutine error !" .. err)
	end
end

function inherit(super, service_name)
	local cls = {
		_PACKET_HANDLE = {},
		_NAME = service_name,
	}
	for k,v in pairs(super) do
		cls[k] = v
	end
	return cls
end

return service
