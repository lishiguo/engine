package.path = package.path .. ";./?.lua;./script/?.lua;"

_G._require = _G._require or require
local _LOADED = {}

local function my_require(mod_name)
	if not _LOADED[mod_name] then
		local mod = _G._require(mod_name)
		_LOADED[mod_name] = mod
	end
	return _LOADED[mod_name]
end

require = my_require

local shengine = require "shengine"

function entry__(...)
	return shengine.entry__(...)
end

require "initial"

print "[lua_engine]:load ok !"
