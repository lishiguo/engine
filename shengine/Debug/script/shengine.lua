local super = _shengine
local shengine = {}
local self = shengine
setmetatable(shengine, {__index = super})

_G._GAME = _G._GAME or {_DATA = {}}
_G._GAME._CLASS = {}
local _CLASS = _G._GAME._CLASS
local _DATA = _G._GAME._DATA
running_thread = 0
running_handle = 0

function shengine.entry__(cls_name, func_name, service_handle, ...)
	local cls = _CLASS[cls_name]
	if not cls then
		print('[Error]:class ' .. cls_name .. ' not found !')
		return
	end
	if not cls[func_name] then
		print('[Error]:class ' .. cls_name .. ' function ' .. func_name .. ' not found !')
		return
	end
	running_handle = service_handle
	return cls[func_name](service_handle, ...)
end

function shengine.register_class(cls)
	local cls_name = cls._NAME
	assert(cls_name)
	assert(_CLASS[cls_name] == nil)
	_CLASS[cls_name] = cls
end

function shengine._get_class(cls_name)
	return cls_name and _CLASS[cls_name]
end

function shengine.create_data(handle, cls_name)
	assert(_DATA[handle] == nil)
	_DATA[handle] = {
		_NAME = cls_name, 
		_HANDLE = handle, 
		_DATA = {}, 
		_CO = {
			_serial = 1, 
			co_list = {}
			}
		}
end

function shengine.get_data(handle)
	if handle and _DATA[handle] then
		return _DATA[handle]._DATA
	end
end

function shengine._get_co_data(handle)
	return handle and _DATA[handle]._CO.co_list
end

function shengine.get_clsname(handle)
	if handle and _DATA[handle] then
		return _DATA[handle]._NAME
	end
end

function shengine.remove_data(handle)
	assert(_DATA[handle])
	_DATA[handle] = nil
end

function shengine.gen_coid(handle)
	assert(handle)
	local co_data = _DATA[handle]._CO
	local coid = co_data._serial
	co_data._serial = co_data._serial + 1
	if co_data._serial == 0 then
		co_data._serial = 1
	end
	return coid
end

local co_pool = {}

local function co_create(f)
	local co = table.remove(co_pool)
	if co == nil then
		co = coroutine.create(
			function(...)
				f(...)
				while true do
					f = nil
					f = coroutine.yield() 
					f(coroutine.yield())
				end
			end)
	else
		coroutine.resume(co, f)
	end
	return co
end

function shengine.fork(handle, f, ...)
	assert(running_thread == 0, "cannot call shengine.fork in a thread !")
	--assert(running_handle == 0, "cannot call shengine.fork in a thread ex !")
	local co_list = shengine._get_co_data(handle)
	local coid = self.gen_coid(handle)
	local co
	local watchf = function(...)
		f(...)
		running_thread = 0
		running_handle = 0
		co_list[coid] = nil
		co_pool[#co_pool+1] = co
	end
	co = co_create(watchf)--coroutine.create(watchf)
	assert(co_list[coid] == nil)
	co_list[coid] = co
	running_thread = coid
	running_handle = handle
	local ret,err = coroutine.resume(co, ...)
	if not ret then
		co_list[coid] = nil
		print("Error: shengine.fork error !" .. err)
	end
end

function shengine.call(target_handle, func_name, ...)
	assert(running_thread ~= 0, "shengine.call must be call in a thread !")
	assert(running_handle ~= 0, "shengine.call must be call in a thread ex !")

	--[[local target_data = self.get_data(target_handle)
	if target_data then
		local cls_name = self.get_clsname(target_handle)
		local cls = self._get_class(cls_name)
		local ok,ret = pcall(cls[func_name], target_handle, ...)
		if ok then
			return ret
		else
			print("==>>Error: shengine.call error !\n" .. ret)
		end
	else--]]
		self.remote_call(running_handle, running_thread, target_handle, func_name, ...)
		running_thread = 0
		running_handle = 0
		return coroutine.yield()
	--end
	
end

function shengine.send(target_handle, func_name, ...)
	--[[local target_data = self.get_data(target_handle)
	if target_data then
		local cls_name = self.get_clsname(target_handle)
		local cls = self._get_class(cls_name)
		local ok = pcall(cls[func_name], target_handle, ...)
		if not ok then
			print("==>>Error: shengine.send error !\n" .. ret)
		end
	else--]]
		self.remote_call(-1, -1, target_handle, func_name, ...)
	--end
end

function shengine.send_packet(target_handle, packet_name, packet)
	assert(false, "shengine.send_packet not implement !")
end

function shengine.new_service(handle, new_handle, class_name, process_interval)
	assert(running_thread ~= 0, "shengine.new_service must be call in a thread !")
	assert(running_handle ~= 0, "shengine.new_service must be call in a thread ex !")
	super.new_service(handle, running_thread, new_handle, class_name, process_interval)
	running_thread = 0
	running_handle = 0
	return coroutine.yield()
end

return shengine
