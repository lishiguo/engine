local super = require "service"
local service_mgr = inherit(super, "service_mgr")
local self = service_mgr
local shengine = require "shengine"
local handle_mgr = require "handle_mgr"
local math = math

function service_mgr.start(handle)
	super.start(handle, self)
	shengine.fork(handle, function()
		local data = shengine.get_data(handle)
		data.actor_list = {}
		local db_handle = handle_mgr.gen_dbhandle()
		local db_ret_handle = shengine.new_service(handle, db_handle, "db_service", 20)
		assert(db_ret_handle == db_handle)
		local ok,msg = shengine.call(db_handle, "start")
		
		for i=1,500 do
			local actor_handle = handle_mgr.gen_actorhandle()
			local ret_handle = shengine.new_service(handle, actor_handle, "actor_service", 20)
			assert(ret_handle == actor_handle)
			shengine.send(actor_handle, "start")
			table.insert(data.actor_list, actor_handle)
		end

		--shengine.exit()

		-- for k,v in ipairs(data.actor_list) do
		-- 	shengine.kick(v)
		-- end
		
		data._RUN = true
		data.last_test_time = 0
	end)
end

local msgcnt = 0
function service_mgr.process(handle)
	local data = shengine.get_data(handle)
	if data and data._RUN then
		for i=1,400 do
			local sz = #data.actor_list
			local actor_1 = data.actor_list[math.random(1, sz)]
			local actor_2 = data.actor_list[math.random(1, sz)]
			shengine.send(actor_1, "req_testmsg", actor_2, shengine.tick())
		end
		msgcnt = msgcnt + 100
		if msgcnt%20000 == 0 then
			print ("==================" .. msgcnt)
		end
	end
end

shengine.register_class(service_mgr)

return service_mgr
