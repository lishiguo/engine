#include "engine/base.h"
#include "engine/cheetah.h"
#include "engine/timer_service.h"
#include "engine/task_service.h"
#include "engine/thread_service.h"
#include "engine/service_center.h"

static int32 addtaskn = 0;
static int32 dotaskn = 0;

void do_task(void* ctx, void* param) {
	//printf("do_task !\n");
	dotaskn++;
}

void add_task(void* ctx, void* param) {
	int taskn = 102400;
	int i;
	cheetah_handle handle;
	cheetah_task* task;
	for ( i = 0; i < taskn; i++) {
		handle = *(cheetah_handle*)param;
		task = cheetah_task_create(0, do_task, 0, 0);
		cheetah_task_service_send_task(handle, task);
	}
	addtaskn += taskn;
	//printf("add_task %d !\n", taskn);
}

int main()
{
	

	cheetah_init(32);

	for (int32 i = 0; i < 8; i++)
	{
		cheetah_handle handle = i;
		cheetah_tq* tq = cheetah_tq_create(10240000, handle);
		cheetah_timer* timer = cheetah_timer_create(10, add_task, 0, &tq->handle_);
		cheetah_service* s = cheetah_service_create(handle, tq, timer);
		cheetah_service_center_add_service(s);
	}

	cheetah_start();
	thread_service_create_threads(30);

	while (1) {
		cheetah_sleep(1000);
	}

    return 0;
}

