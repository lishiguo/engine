#pragma once
#include "base.h"
#include "service.h"
#include "timer.h"

typedef struct cheetah_service_cache_node_list {
	struct cheetah_service* head_;
	struct cheetah_service* tail_;
	cheetah_lock lock_;
} cheetah_service_cache_node_list;

typedef struct cheetah_service_center {
	cheetah_service_cache_node_list service_cache_node_list_;
	cheetah_service* service_hnode_list_[TQ_LIST_HASH_SIZE];
	cheetah_service* delay_free_list_;
	cheetah_timer* timer_;
} cheetah_service_center;

int32 cheetah_service_center_init();
int32 cheetah_service_center_add_service(cheetah_service* s);
int32 cheetah_service_center_remove_service(cheetah_handle handle);
void cheetah_task_center_start();
