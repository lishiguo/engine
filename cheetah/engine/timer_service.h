#pragma once
#include "timer.h"

struct cheetah_timer_service {
	cheetah_timer_list list_;
	cheetah_timer_cache_list cache_list_;
};

void cheetah_timer_service_init();
void cheetah_timer_service_add_timer(cheetah_timer* t);
void cheetah_timer_service_start();

typedef struct cheetah_timer_service cheetah_timer_service;
