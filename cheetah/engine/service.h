#pragma once
#include "base.h"
#include "task_queue.h"
#include "timer.h"

typedef struct cheetah_service {
	cheetah_handle handle_;
	cheetah_tq* tq_;
	cheetah_timer* timer_;
	struct cheetah_service* next_;
} cheetah_service;

cheetah_service* cheetah_service_create(cheetah_handle handle, cheetah_tq* tq, cheetah_timer* timer);
