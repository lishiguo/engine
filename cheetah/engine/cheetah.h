#pragma once

#include "base.h"

void cheetah_init(uint32 thread_max);
void cheetah_start();
void cheetah_tick();

