#pragma once
#include "task_queue.h"

#define DEFAULT_TQ_LIST_SIZE 1024
#define THREAD_MAX 256
#define TASK_EXEC_WARNNING_TIME 100
#define TASK_EXEC_ASSERT_TIME 15000

struct cheetah_timer;

struct cheetah_tq_node {
	cheetah_tq* tq_;
	struct cheetah_tq_node* next_;
	int32 new_;
};

struct cheetah_thread_tinfo {
	cheetah_task* task_;
	uint32 finish_count_;
	struct cheetah_tq_node* node_;
	uint64 proc_time_;
};

struct cheetah_tq_list {
	struct cheetah_tq_node* head_;
	struct cheetah_tq_node* tail_;
};

struct cheetah_cache_tq_list {
	struct cheetah_tq_node* head_;
	struct cheetah_tq_node* tail_;
	cheetah_lock lock_;
};

struct cheetah_task_list {
	struct cheetah_task* head_;
	struct cheetah_task* tail_;
	cheetah_lock lock_;
};

struct cheetah_tq_hash_node {
	cheetah_tq* tq_;
	struct cheetah_tq_hash_node* next_;
};

typedef struct cheetah_cache_tq_list cheetah_task_tq_list;

struct cheetah_task_service {
	struct cheetah_tq_list tq_list_;
	struct cheetah_cache_tq_list cache_tq_list_;
	cheetah_task_tq_list task_tq_list_;
	struct cheetah_tq_hash_node* tq_hash_list_[TQ_LIST_HASH_SIZE]; 
	struct cheetah_thread_tinfo* cur_task_list_;
	struct cheetah_task_list cache_task_list_;
	struct cheetah_timer* timer_;
	int32 thread_max_;
};

typedef struct cheetah_thread_tinfo cheetah_thread_tinfo;
typedef struct cheetah_tq_node cheetah_tq_node;
typedef struct cheetah_tq_list cheetah_tq_list;
typedef struct cheetah_cache_tq_list cheetah_cache_tq_list;
typedef struct cheetah_task_list cheetah_task_list;
typedef struct cheetah_task_service cheetah_task_service;
typedef struct cheetah_tq_hash_node cheetah_tq_hash_node;

void cheetah_task_service_init(int32 thread_max);
void cheetah_task_service_add_tq(cheetah_tq* tq);
void cheetah_task_service_send_task(cheetah_handle handle, cheetah_task* task);
cheetah_task* cheetah_task_service_task_fetch(int32 tid);
void cheetah_task_service_task_finish(cheetah_task* t, int32 tid);
void cheetah_task_service_start();
void cheetah_task_service_stop();

//todo timer for dispatch task; tick_add_task
//todo timer for check deadly task tick_check_cur_tasks

