#include "cheetah.h"
#include "task_service.h"
#include "timer_service.h"
#include "service_center.h"

void cheetah_init(uint32 thread_max) {
	cheetah_timer_service_init();
	cheetah_task_service_init(thread_max);
	cheetah_service_center_init();
}

void cheetah_start() {
	cheetah_task_service_start();
	cheetah_task_center_start();
	cheetah_timer_service_start();
}

void cheetah_tick() {
	//cheetah_timer_service_tick();
}
