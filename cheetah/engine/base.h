#pragma once
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "type.h"

#define __CHEETAH_WIN32__

#if defined(__CHEETAH_WIN32__)
#define __sync_lock_test_and_set(plock, v) InterlockedExchange(plock, v)
#define __sync_lock_release(plock) InterlockedExchange(plock, 0)
#define __sync_add_and_fetch(plock, v) InterlockedIncrement(plock)
#define __sync_sub_and_fetch(plock, v) InterlockedDecrement(plock)
typedef long cheetah_lock;
typedef long cheetah_ref;
#define THREAD_ID unsigned long
#else
typedef int32 cheetah_lock;
typedef int32 cheetah_ref;
#define THREADID pthread_t
#endif

#define ATOM_LOCK(L) while(__sync_lock_test_and_set(L, 1));
#define ATOM_UNLOCK(L) __sync_lock_release(L);
#define ATOM_ADD(N, v) __sync_add_and_fetch(N, v);
#define ATOM_SUB(N, v) __sync_sub_and_fetch(N, v);
#define REF_RETAIN(R) __sync_add_and_fetch(R, 1);
#define REF_RELEASE(R) __sync_sub_and_fetch(R, 1);

#define cheetah_sleep Sleep

uint64 cheetah_time_s();
uint64 cheetah_time_ms();

typedef void(*task_cb)(void* ctx, void* param);
typedef task_cb timer_cb;
typedef void(*thread_frame_func)(uint32 thread_id);

#define TQ_LIST_HASH_SIZE 2
