#include "service.h"

cheetah_service* cheetah_service_create(cheetah_handle handle, cheetah_tq* tq, cheetah_timer* timer) {
	cheetah_service* s = (cheetah_service*)malloc(sizeof(cheetah_service));
	assert(s);
	s->handle_ = handle;
	s->tq_ = tq;
	s->timer_ = timer;
	s->next_ = 0;
	return s;
}
