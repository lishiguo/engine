#include "base.h"
#include "thread_service.h"
#include "task_service.h"

#define FRAME_INTERVAL 4

void thread_frame(uint32 thread_id) {
	cheetah_task* t;
	while (t = cheetah_task_service_task_fetch(thread_id)) {
		t->cb_(t->ctx_, t->param_);
		cheetah_task_service_task_finish(t, thread_id);
	}
}

void thread_service_create_threads(uint32 thread_count) {
	uint32 i;
	cheetah_thread* thread;
	for (i = 0; i < thread_count; i++)
	{
		thread = cheetah_thread_create(i, thread_frame, FRAME_INTERVAL);
		cheetah_thread_start(thread);
	}
}


