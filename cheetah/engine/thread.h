#pragma once
#include "base.h"

#define THREAD_STATE_INIT		0
#define THREAD_STATE_RUNNING	1
#define THREAD_STATE_TOPAUSE	2
#define THREAD_STATE_PAUSE		3
#define THREAD_STATE_TOEXIT		4
#define THREAD_STATE_EXIT		5

#define THREAD_WORKSTATE_SUSPEND	0		//hang up
#define THREAD_WORKSTATE_HUNGRY		1		//[0%, 40%)
#define THREAD_WORKSTATE_NORMAL		2		//[40, 80%)
#define THREAD_WORKSTATE_BUSY		3		//[80%, 100%)
#define THREAD_WORKSTATE_OVERLOAD	4		//[100%, 100+%)

struct cheetah_thread {

	
	int32 state_;
	int32 work_state_;
	uint32 tick_interval_;
	uint32 round_interval_;
	uint64 round_tick_time_;
	uint64 round_work_time_;
	THREAD_ID thread_id_;
	uint32 thread_index_;
	thread_frame_func frame_func_;
#ifdef __CHEETAH_WIN32__
	HANDLE handle_;
#endif
};
typedef struct cheetah_thread cheetah_thread;

cheetah_thread* cheetah_thread_create(uint32 thread_index, thread_frame_func frame_func, uint32 frame_interval);

int32 cheetah_thread_join(cheetah_thread* thread);

int32 cheetah_thread_start(cheetah_thread* thread);

int32 cheetah_thread_stop(cheetah_thread* thread);

int32 cheetah_thread_pause(cheetah_thread* thread);

int32 cheetah_thread_resume(cheetah_thread* thread);
