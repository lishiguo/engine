#pragma once
#include "base.h"
#include <string.h>
#include <stdlib.h>

struct cheetah_timer {
	uint32 interval_;
	uint64 next_hit_time_;
	void* ctx_;
	timer_cb cb_;
	void* param_;
	int32 release_;
	cheetah_ref ref_;
	struct cheetah_timer* next_;
};

struct cheetah_timer_list {
	struct cheetah_timer* head_;
	struct cheetah_timer* tail_;
};

struct cheetah_timer_cache_list {
	struct cheetah_timer* head_;
	struct cheetah_timer* tail_;
	cheetah_lock lock_;
};

struct cheetah_timer* cheetah_timer_create(uint32 interval, timer_cb cb, void* ctx, void* param);

typedef struct cheetah_timer cheetah_timer;
typedef struct cheetah_timer_list cheetah_timer_list;
typedef struct cheetah_timer_cache_list cheetah_timer_cache_list;
