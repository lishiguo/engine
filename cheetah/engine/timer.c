#include "timer.h"

cheetah_timer* cheetah_timer_create(uint32 interval, timer_cb cb, void* ctx, void* param) {
	cheetah_timer* t = (cheetah_timer*)malloc(sizeof(cheetah_timer));
	t->interval_ = interval;
	t->next_hit_time_ = cheetah_time_ms() + t->interval_;
	t->ctx_ = ctx;
	t->cb_ = cb;
	t->param_ = param;
	t->next_ = 0;
	t->release_ = 0;
	t->ref_ = 0;
	return t;
}
