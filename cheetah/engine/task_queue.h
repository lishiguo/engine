#pragma once

#include "base.h"

#define TASK_NOBLOCK 0
#define TASK_BLOCK 1
#define TASK_BLOCKTASKMAX 4

struct cheetah_task {
	cheetah_handle handle_;
	void* ctx_;
	void* param_;
	task_cb cb_;
	int32 type_;
	struct cheetah_task* next_;
};

struct cheetah_tq {
	cheetah_handle handle_;
	struct cheetah_task* head_;
	struct cheetah_task* tail_;
	uint32 frame_task_max_;
	int32 release_;
	cheetah_ref ref_;
	cheetah_lock lock_;
};

typedef struct cheetah_task cheetah_task;
typedef struct cheetah_tq cheetah_tq;

cheetah_task* cheetah_task_create(int32 type, task_cb cb, void* ctx, void* param);
cheetah_tq* cheetah_tq_create(uint32 frame_task_max, cheetah_handle handle);
void cheetah_tq_task_push(cheetah_tq* q, cheetah_task* t);
cheetah_task* cheetah_tq_task_pop(cheetah_tq* q);
